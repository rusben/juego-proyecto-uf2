# Juego Poyecto UF2

## How to compile?
```bash
# Go into the project source folder (src)
$ cd juego-proyecto-uf2/src

# Compile the main class
$ javac net/xeill/elpuig/Juegos2020.java

# Run the main
$ java net.xeill.elpuig.Juegos2020
```
