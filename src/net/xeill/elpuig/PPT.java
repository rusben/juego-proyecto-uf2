package net.xeill.elpuig;

import java.util.Scanner;

class PPT {


  public void start() {
    System.out.println("Has seleccionado jugar a Piedra, Papel o Tijera");
    System.out.println("-------------------------------------------------");
    System.out.println("Comienza el juego");
  }

  public int player() {
    Scanner input = new Scanner(System.in);
    int seleccion_usuario = 1;
    System.out.println("Indica piedra papel o tijera (1,2 o 3)");
    try {
      String seleccion_usuario_string = input.nextLine();
      seleccion_usuario = Integer.parseInt(seleccion_usuario_string);
    } catch (Exception e) {
      return player();
    }
    return seleccion_usuario;
  }

    public int machine() {

      System.out.println("La máquina está escogiendo...");
      return (int)(Math.random()*3)+1; //Escoge un número del 0 al 2 pero le suma 1 así las opciones son 123

    }

    public void result(int seleccion_usuario, int seleccion_pc) {
      System.out.println(" La máquina ha escogido: ");
      switch ( seleccion_pc ) {
        case 1:
          System.out.println("Piedra");
          switch ( seleccion_usuario ) {
            case 1: System.out.println("Empate"); break;
            case 2: System.out.println("Ganas"); break;
            case 3: System.out.println("Pierdes"); break;
          }
          break;

        case 2:
          System.out.println("Papel");
          switch ( seleccion_usuario ) {
            case 1: System.out.println("Pierdes"); break;
            case 2: System.out.println("Empate"); break;
            case 3: System.out.println("Ganas"); break;
          }
          break;

        case 3:
          System.out.println("Tijeras");
          switch ( seleccion_usuario ){
            case 1: System.out.println("Ganas"); break;
            case 2: System.out.println("Pierdes"); break;
            case 3: System.out.println("Empatas"); break;
          }
          break;

      }

    }

}
