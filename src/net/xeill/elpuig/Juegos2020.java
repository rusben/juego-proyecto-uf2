package net.xeill.elpuig;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Juegos2020{

    public static void main(String[] args) {
      run();
    }

    public static void run() {
      Scanner input = new Scanner(System.in);
      IO io = new IO();
      PPT ppt = new PPT();
      boolean salir = false;
      int opcion = 1; //Guardaremos la opcion del usuario

      while (!salir) {

          io.mainMenu();

              try {
                opcion = io.selectMainMenu();
              } catch (Exception e) {
                // Hay que decidir qué hacer
              }

              switch (opcion) {
                  case 1:
                      boolean ppti = true;

                      while (ppti == true) {

                        ppt.start();
                      //  int seleccion_usuario = ppt.player();
                      //  int seleccion_pc = ppt.machine();
                      //  ppt.result(seleccion_usuario, seleccion_pc);
                        ppt.result(ppt.player(), ppt.machine());

                        System.out.println("Quieres salir de este juego?: [si/no] ");
                        String salir_juego1 = input.nextLine();

                      //  System.out.println(salir_juego1);

                        if (salir_juego1.equals("si") ) {
                          System.out.println("De vuelta al menu...");
                          ppti = false;
                        }
                      }


                  case 2:
                      System.out.println("Has seleccionado jugar a Par o Impar");
                      break;
                  case 3:
                      System.out.println("Has seleccionado jugar a Buscaminas");
                      break;
                  case 4:
                      System.out.println("Adios");
                      salir = true;
                      break;
                  default:
                      System.out.println("Solo números entre 1 y 4");
              }
          // } catch (Exception e) {
          //     System.out.println("Debes insertar un número");
          //     run();
          // }
      }

    }

}
