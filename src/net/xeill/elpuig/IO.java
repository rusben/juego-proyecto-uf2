package net.xeill.elpuig;

import java.util.Scanner;

class IO {
  Scanner input = new Scanner(System.in);

  public void mainMenu() {
    System.out.println("1. Piedra, Papel o Tijera");
    System.out.println("2. Par o Impar");
    System.out.println("3. Buscaminas");
    System.out.println("4. Salir");
  }

  public int selectMainMenu() {
    System.out.println("Escribe una de las opciones");
    return input.nextInt();
  }


}
